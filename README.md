# Project Livestream

## Requirements

- Docker
- Docker Compose

## Run locally

```shell
docker run --rm -w /app -e "DATABASE_URL=dummy" -v "$(pwd):/app" composer install
cp ./build/docker-compose.yml.dist ./docker-compose.yml
docker-compose up -d
```

## Run php commands

To run php commands, start bash in container:
```shell
docker exec -ti livestream-test_php_1 bash
```

## Add/remove composer dependencies

```shell
docker run --rm -w /app -v "$(pwd):/app" composer require <package name>
```

## Stop environment
```shell
docker-compose down
```
